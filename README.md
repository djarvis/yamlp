# YAMLP

YAMLP is a preprocessor for YAML. Given a YAML file with delimited references such as:

    ---
    a: A 
    b: $a$ B 
    c: $b$ C $b$
    d: $a$ $b$ $c$ D $a$
    e:
      f: 
        g: G
    h: H and $e.f.g$


This will write a new YAML file to standard output with the references substituted using corresponding YAML path values from within the document:

    ---
    a: "A"
    b: "A B"
    c: "A B C A B"
    d: "A A B A B C A B D A"
    e:
      f:
        g: "G"
    h: "H and G"


This is similar to the template mechanism used by [pandoc](http://pandoc.org/) with the addition of recursive delimited references.

# Requirements

Install the following software packages:

* [Java Development Kit](https://openjdk.java.net/install) 1.8.0 (or newer)
* [Apache Maven](https://maven.apache.org/) 3.6.0 (or newer)

Make sure the commands `java` and `mvn` are working before attempting to build the application.

# Build

Build the project as follows:

``` bash
mkdir -p $HOME/dev/java
cd $HOME/dev/java
git clone https://bitbucket.org/djarvis/yamlp
cd yamlp
mvn package
```

The file `target/yamlp.jar` is built.

# Install

Install the program by copying it into a directory. For example:

``` bash
mkdir -p $HOME/bin
cp target/yamlp.jar $HOME/bin
```

The file `yamlp.jar` is installed into `$HOME/bin`.

# Usage

Example usages:

``` bash
java -jar $HONE/bin/yamlp.jar < variables.yaml > processed.yaml
```

These examples show to to run the YAML preprocessor, reading a file from standard input (e.g., `variables.yaml`) and writing the processed version of the file to standard output (e.g., `processed.yaml`).

# Options

The command line arguments include:

* `--help` -- show the usage information
* `--input` -- path to the YAML file to process (standard input)
* `--regex` -- set expression for matching variables (`(\\$(.*?)\\$)`)
* `--separator` -- set the separator for delimited paths (`.`)
* `--quiet` -- suppress writing missing references to standard error

## regex

The regular expression syntax allows a variety of syntaxes to be used when matching variable name references, such as:

* `(\$(.*?)\$)` -- matches `$...$`
* `(\$\{(.*?)\})` -- matches `${...}`

The open and closing parentheses---`(` and `)`---are mandatory. The `?` indicates a non-greedy match. Without the `?`, multiple variable references would not match. For example, `name: $var.1$ $var.2$` would be treated as a variable named `var.1$ $var.2` instead of two distinct variables `var.1` and `var.2`.

Wrap the regular expression in single quotes when running the command, such as:

``` bash
java -jar $HOME/bin/yamlp.jar --regex '(\$(.*?)\$)' < variables.yaml
java -jar $HOME/bin/yamlp.jar --regex '(\$\{(.*?)\})' < variables.yaml
java -jar $HOME/bin/yamlp.jar --regex '(\{\{(.*?)\}\})' < variables.yaml
```

The single quote prevents the shell from expanding the regex argument into a list of file names.

# Errors

Missing delimited references are written to standard error.

