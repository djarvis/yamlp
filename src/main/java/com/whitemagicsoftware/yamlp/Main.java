/*
 * The MIT License
 *
 * Copyright 2017 White Magic Software, Ltd..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.whitemagicsoftware.yamlp;

import com.whitemagicsoftware.yamlp.parser.YamlParser;
import java.io.IOException;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

/**
 * This program loads a YAML document into memory, scans for variable
 * declarations, then substitutes any self-referential values back into the
 * document. Its output is the given YAML document without any variables.
 * Variables in the YAML document are denoted using a bracketed dollar symbol
 * syntax. For example: $field.name$. Some nomenclature to keep from going
 * squirrelly, consider:
 *
 * <pre>
 *   root:
 *     node:
 *       name: $field.name$
 *   field:
 *     name: Alan Turing
 * </pre>
 *
 * The various components of the given YAML are called:
 *
 * <ul>
 * <li><code>$field.name$</code> - delimited reference</li>
 * <li><code>field.name</code> - reference</li>
 * <li><code>name</code> - YAML field</li>
 * <li><code>Alan Turing</code> - (dereferenced) field value</li>
 * </ul>
 *
 * @author White Magic Software, Ltd.
 */
public class Main {

  /**
   * Constructs the application entry point with post-parsed command line
   * options.
   */
  private Main() {
  }

  public void run( final Configuration configuration ) throws IOException {
    final YamlParser parser = new YamlParser( configuration );

    parser.process();
  }

  /**
   * Processes the YAML document specified on the command line.
   *
   * @param args Contains a filename.
   */
  public static void main( final String[] args ) {
    final Configuration configuration = new Configuration();
    final CmdLineParser clp = new CmdLineParser( configuration );

    try {
      clp.parseArgument( args );

      Main main = new Main();
      main.run( configuration );
    }
    catch( final CmdLineException e ) {
      configuration.usage( clp );
    }
    catch( final IOException e ) {
      configuration.log( e.getLocalizedMessage() );
    }
  }
}
