/*
 * The MIT License
 *
 * Copyright 2017 White Magic Software, Ltd..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.whitemagicsoftware.yamlp;

import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author White Magic Software, Ltd.
 */
public class Configuration {

  /**
   * Matches variables delimited by dollar symbols. The outer group is necessary
   * for substring replacement of delimited references.
   */
  private final static String DEFAULT_REGEX = "(\\$(.*?)\\$)";

  /**
   * Separates variable nodes (e.g., the dots in $root.node.var$).
   */
  private final static String DEFAULT_SEPARATOR = ".";

  /**
   * Used to find fields that have variables in their values.
   */
  private String regex = DEFAULT_REGEX;

  /**
   * Variable name separator given on the command line.
   */
  private String separator = DEFAULT_SEPARATOR;

  /**
   * File name given on the command line.
   */
  private String inputFilename;

  /**
   * File name given on the command line.
   */
  private String outputFilename;

  /**
   * Set to false to suppress logging to standard error.
   */
  private boolean logging = true;

  /**
   * Set to true to write the variables to standard error.
   */
  private boolean showVariables;

  /**
   * Export format.
   */
  private FileType exportFormat;

  /**
   * Returns the name of the YAML file to parse. This is set by parsing the
   * command line.
   *
   * @return The filename to read.
   */
  public String getInputFilename() {
    return this.inputFilename;
  }

  public InputStream getInputStream() throws IOException {
    final String filename = getInputFilename();

    return isEmpty( filename )
        ? System.in
        : createFileInputStream( filename );
  }

  /**
   * Open a file.
   *
   * @param filename The file name to open.
   * @throws IOException Could not open the file.
   */
  private InputStream createFileInputStream( final String filename )
      throws IOException {
    return new FileInputStream( new File( filename ) );
  }

  /**
   * Sets name of the YAML file to parse.
   *
   * @param inputFilename File containing a YAML document.
   */
  @Option(name = "--input", metaVar = "input.yaml", usage = "YAML filename")
  public void setInputFilename( final String inputFilename ) {
    this.inputFilename = inputFilename;
  }

  /**
   * Returns the name of the YAML file to write. This is set by parsing the
   * command line.
   *
   * @return The filename to write.
   */
  public String getOutputFilename() {
    return this.outputFilename;
  }

  /**
   * Returns the regular expression used to match variable names.
   *
   * @return A non-null, possibly empty string.
   */
  public String getRegex() {
    return this.regex;
  }

  @Option(name = "--regex",
      metaVar = DEFAULT_REGEX,
      usage = "Regular expression to match variables")
  private void setRegex( final String regex ) {
    this.regex = regex;
  }

  /**
   * Returns the character used to separate YAML paths within delimited
   * references. This will return only the first character of the command
   * line parameter, if the default is overridden.
   *
   * @return A period by default.
   */
  public char getSeparator() {
    return this.separator.charAt( 0 );
  }

  @Option(name = "--separator",
      metaVar = DEFAULT_SEPARATOR,
      usage = "Default variable expression separator")
  private void setSeparator( final String separator ) {
    this.separator = separator;
  }

  /**
   * Answers whether to write to standard error.
   *
   * @return true Log errors and warnings.
   */
  public boolean isLogging() {
    return this.logging;
  }

  /**
   * Writes the message to standard error if logging is enabled.
   *
   * @param message Conditionally written to standard error.
   */
  public void log( final String message ) {
    if( isLogging() ) {
      stderr( message );
    }
  }

  /**
   * Writes the message to standard error without considering whether logging
   * is disabled.
   *
   * @param message Written to standard error.
   */
  public void stderr( final String message ) {
    System.err.println( message );
  }

  /**
   * Shows the command line parameters.
   *
   * @param clp CLI option parser.
   */
  public void usage( final CmdLineParser clp ) {
    stderr( "Usage:" );
    clp.printUsage( System.err );
  }

  /**
   * Returns true iff s is null or empty.
   *
   * @param s The string to check for content.
   * @return true The string has no content (or is null).
   */
  private boolean isEmpty( final String s ) {
    return s == null || s.isEmpty();
  }
}
