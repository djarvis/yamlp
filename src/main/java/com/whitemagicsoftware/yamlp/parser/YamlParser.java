/*
 * The MIT License
 *
 * Copyright 2017 White Magic Software, Ltd..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.whitemagicsoftware.yamlp.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.whitemagicsoftware.yamlp.Configuration;
import com.whitemagicsoftware.yamlp.resolvers.ResolverYAMLFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <p>
 * This program loads a YAML document into memory, scans for variable
 * declarations, then substitutes any self-referential values back into the
 * document. Its output is the given YAML document without any variables.
 * Variables in the YAML document are denoted using a bracketed dollar symbol
 * syntax. For example: $field.name$. Some nomenclature to keep from going
 * squirrely, consider:
 * </p>
 *
 * <pre>
 *   root:
 *     node:
 *       name: $field.name$
 *   field:
 *     name: Alan Turing
 * </pre>
 * <p>
 * The various components of the given YAML are called:
 *
 * <ul>
 * <li><code>$field.name$</code> - delimited reference</li>
 * <li><code>field.name</code> - reference</li>
 * <li><code>name</code> - YAML field</li>
 * <li><code>Alan Turing</code> - (dereferenced) field value</li>
 * </ul>
 *
 * @author White Magic Software, Ltd.
 */
public class YamlParser {

  private final static int GROUP_DELIMITED = 1;
  private final static int GROUP_REFERENCE = 2;

  /**
   * Compiled regex.
   */
  private Pattern pattern;

  /**
   * Start of the Universe.
   */
  private ObjectNode documentRoot;

  /**
   * Command-line options parsed using third-party library.
   */
  private Configuration configuration;

  /**
   * Prefer JsonPointer.SEPARATOR, but Jackson YAML uses magic values.
   */
  private final static char SEPARATOR_YAML = '/';

  /**
   * Map of references to dereferenced field values.
   */
  private Map<String, String> references;

  public YamlParser( final Configuration configuration ) {
    setConfiguration( configuration );
  }

  /**
   * Returns the given string with all the delimited references swapped with
   * their recursively resolved values.
   *
   * @param text The text to parse with zero or more delimited references to
   *             replace.
   * @return The substituted value.
   */
  public String substitute( String text ) {
    final Matcher matcher = patternMatch( text );
    final Map<String, String> map = getReferences();

    while( matcher.find() ) {
      final String key = matcher.group( GROUP_DELIMITED );
      final String value = map.get( key );

      if( value == null ) {
        missing( text );
      }
      else {
        text = text.replace( key, value );
      }
    }

    return text;
  }

  /**
   * Runs before the processing begins. No default behaviour.
   */
  public void preprocess() {
  }

  /**
   * Runs after processing ends. Writes the variables to standard error if the
   * option to do so was given on the command line.
   *
   * @throws IOException Could not write the document (to standard output).
   */
  public void postprocess() throws IOException {
    writeDocument();
  }

  /**
   * Open a file specified on the command line and process its YAML contents.
   * The underlying file stream is closed on success or error.
   *
   * @throws IOException Could not read the file contents.
   */
  public void process() throws IOException {
    preprocess();
    process( getInputStream() );
    postprocess();
  }

  /**
   * Opens the configured input stream (could be a file or standard input).
   *
   * @throws IOException Could not open the stream for reading.
   */
  private InputStream getInputStream() throws IOException {
    return getConfiguration().getInputStream();
  }

  /**
   * Read and process the contents from an open stream. The stream remains open
   * after calling this method, regardless of success or error. This can be
   * considered the main entry point to the program.
   *
   * @param in The stream with a YAML document to process.
   * @throws IOException Could not read the file contents.
   */
  public void process( final InputStream in ) throws IOException {
    process( readDocument( in ) );
  }

  /**
   * Iterate over a given root node (at any level of the tree) and process each
   * leaf node.
   *
   * @param root A node to process.
   */
  private void process( final JsonNode root ) {
    root.fields().forEachRemaining( this::process );
  }

  /**
   * Process the given field, which is a named node. This is where the
   * application does the up-front work of mapping references to their fully
   * recursively dereferenced values.
   *
   * @param field The named node.
   */
  private void process( final Entry<String, JsonNode> field ) {
    final JsonNode node = field.getValue();

    if( node.isObject() ) {
      process( node );
    }
    else {
      final JsonNode fieldValue = field.getValue();

      // Only basic data types can be parsed into variable values. For
      // node structures, YAML has a built-in mechanism.
      if( fieldValue.isValueNode() ) {
        try {
          resolve( fieldValue.asText() );
        } catch( StackOverflowError e ) {
          error( "Unresolvable: " + node.textValue() + " = " + fieldValue );
        }
      }
    }
  }

  /**
   * Inserts the delimited references and field values into the cache. This will
   * overwrite existing references.
   *
   * @param fieldValue YAML field containing zero or more delimited references.
   *                   If it contains a delimited reference, the parameter is
   *                   modified with the
   *                   dereferenced value before it is returned.
   * @return fieldValue without delimited references.
   */
  private String resolve( String fieldValue ) {
    final Matcher matcher = patternMatch( fieldValue );

    while( matcher.find() ) {
      final String delimited = matcher.group( GROUP_DELIMITED );
      final String reference = matcher.group( GROUP_REFERENCE );
      final String dereference = resolve( lookup( reference ) );

      fieldValue = fieldValue.replace( delimited, dereference );

      // This will perform some superfluous calls by overwriting existing
      // items in the delimited reference map.
      put( delimited, dereference );
    }

    return fieldValue;
  }

  /**
   * Inserts a key/value pair into the references map. The map retains
   * references and dereferenced values found in the YAML. If the reference
   * already exists, this will overwrite with a new value.
   *
   * @param delimited    The variable name.
   * @param dereferenced The resolved value.
   */
  private void put( String delimited, String dereferenced ) {
    if( dereferenced.isEmpty() ) {
      missing( delimited );
    }
    else {
      getReferences().put( delimited, dereferenced );
    }
  }

  /**
   * Reads the first YAML document from the input stream.
   *
   * @param in The YAML contents.
   * @return The root node for the YAML document.
   * @throws IOException Could not read the stream.
   */
  private ObjectNode readDocument( final InputStream in ) throws IOException {
    setDocumentRoot( (ObjectNode) createObjectMapper().readTree( in ) );
    return getDocumentRoot();
  }

  /**
   * Writes the modified YAML document to standard output.
   */
  private void writeDocument() throws IOException {
    createObjectMapper().writeValue( System.out, getDocumentRoot() );
  }

  /**
   * Called when a delimited reference is dereferenced to an empty string.
   *
   * @param delimited Delimited reference with no derived value.
   */
  private void missing( final String delimited ) {
    warn( String.format( "Missing '%s'.", delimited ) );
  }

  /**
   * Called by all methods that log information.
   *
   * @param level   The level string (WARN/ERROR).
   * @param message The message to log.
   */
  private void log( String level, String message ) {
    getConfiguration().log( String.format( "[%s] %s", level, message ) );
  }

  /**
   * Called by all methods that log warning information.
   *
   * @param message The text to log to standard error.
   */
  private void warn( String message ) {
    log( "WARN", message );
  }

  /**
   * Called by all methods that log error information.
   *
   * @param message The text to log to standard error.
   */
  private void error( String message ) {
    log( "ERROR", message );
  }

  /**
   * Returns a pattern matcher for the given text.
   *
   * @param text The text that contains zero or more instances of a pattern that
   *             can be found using the regular expression.
   */
  private Matcher patternMatch( String text ) {
    return getPattern().matcher( text );
  }

  /**
   * Finds the YAML value for a reference.
   *
   * @param reference References a value in the YAML document.
   * @return The dereferenced value.
   */
  private String lookup( final String reference ) {
    return getDocumentRoot().at( asPath( reference ) ).asText();
  }

  /**
   * Sets the parent node for the entire YAML document tree.
   *
   * @param documentRoot The parent node.
   */
  private void setDocumentRoot( ObjectNode documentRoot ) {
    this.documentRoot = documentRoot;
  }

  /**
   * Returns the parent node for the entire YAML document tree.
   *
   * @return The parent node.
   */
  public ObjectNode getDocumentRoot() {
    return this.documentRoot;
  }

  /**
   * Returns the compiled regular expression pattern used to match delimited
   * references.
   *
   * @return A compiled regex for use with the Matcher.
   */
  private Pattern getPattern() {
    if( this.pattern == null ) {
      this.pattern = Pattern.compile( getRegex() );
    }

    return this.pattern;
  }

  /**
   * Returns the list of references mapped to dereferenced values.
   *
   * @return A map of key/value pairs.
   */
  private Map<String, String> getReferences() {
    if( this.references == null ) {
      this.references = createReferences();
    }

    return this.references;
  }

  /**
   * Converts a reference (not delimited) to a path that can be used to find a
   * value that should exist inside the YAML document.
   *
   * @param reference The reference to convert to a YAML document path.
   * @return The reference with a leading slash and its separator characters
   * converted to slashes.
   */
  private String asPath( final String reference ) {
    return SEPARATOR_YAML + reference.replace(
        getDelimitedSeparator(),
        SEPARATOR_YAML );
  }

  /**
   * Subclasses can override this method to insert their own map.
   *
   * @return An empty HashMap, never null.
   */
  protected Map<String, String> createReferences() {
    return new HashMap<>();
  }

  private YAMLFactory createYAMLFactory() {
    return new ResolverYAMLFactory( this );
  }

  private ObjectMapper createObjectMapper() {
    return new ObjectMapper( createYAMLFactory() );
  }

  private String getRegex() {
    return getConfiguration().getRegex();
  }

  private char getDelimitedSeparator() {
    return getConfiguration().getSeparator();
  }

  private Configuration getConfiguration() {
    return this.configuration;
  }

  private void setConfiguration( Configuration configuration ) {
    this.configuration = configuration;
  }
}
