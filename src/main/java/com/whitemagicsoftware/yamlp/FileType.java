/*
 * The MIT License
 *
 * Copyright 2017 White Magic Software, Ltd..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.whitemagicsoftware.yamlp;

/**
 * List of supported export formats.
 *
 * @author White Magic Software, Ltd.
 */
public enum FileType {
  JSON( ".json", ".jsn" ),
  XML( ".xml" ),
  YAML( ".yaml", ".yml" );

  private String[] extensions;

  FileType( final String... extensions ) {
    setExtensions( extensions );
  }

  /**
   * Returns true if the given filename matches this extension.
   * 
   * @param filename The filename to compare against the extensions.
   * @return true The filename matches this type of file.
   */
  public boolean matches( final String filename ) {
    boolean result = false;
    final String safeFilename = filename == null ? "" : filename.toLowerCase();

    for( final String ext : getExtensions() ) {
      if( safeFilename.endsWith( ext ) ) {
        result = true;
        break;
      }
    }

    return result;
  }

  private void setExtensions( final String[] extensions ) {
    this.extensions = extensions;
  }

  private String[] getExtensions() {
    return extensions;
  }
}
